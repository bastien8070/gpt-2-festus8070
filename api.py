import flask
from flask import request, jsonify
import gpt_2_simple as gpt2
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import sys

firstattemp = True
firstround = True

from flask_cors import CORS, cross_origin
app = flask.Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config["DEBUG"] = True

FirstName = "Bastien"
Engine = "GPT-2"
GPT_VARIANT_NAME="345M"
TRAINING_STEPS="2000"
TRAINING_DATA_SIZE="1MB"
DEVELOPMENT=True
VERSION="v1.0 ALFA"
LAST_UPDATE="Wed 27 Jan, 2021"
model_name = "345M"
def talk(userinput,context,oritext):         
        
        try:
            os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
            tf.reset_default_graph()
        except:
            pass
            #sess = tf.Session(config=config)
        sess = gpt2.start_tf_sess()
        gpt2.load_gpt2(sess)
        #textlength = len(userinput)+50
        textlength=50
        print(textlength)
        text = gpt2.generate(sess, model_name=model_name, return_as_list=True ,prefix=userinput, length=textlength)[0]

        print("WHOLE RESPONSE=",text)
        if context:
            linesnumb = len(context.splitlines())
            text = '\n'.join(text.splitlines()[linesnumb:])
            print("CUT DOWN RESPONSE=",text)


        return text

def communicate(usertalk,context,oritext):
    userinput = usertalk
    new_line = "[others] " + userinput
    if not new_line.endswith(".") or not new_line.endswith("?") or not new_line.endswith("!"):
        new_line += "."

    new_line += " \n[me] "
    if context:
        new_line = context + new_line
    response = []
    i = 0
    answered = False
    while not answered:
        i+=1
        response = []
        if i == 3:
            response.append("I took too long to find an answer lol, sry about that. Hope ill do better next time xD!")
            answered = True     
        responses = talk(new_line,context,oritext).splitlines()
        print("\n\n",new_line,"\n\n",responses,"\n\n")
        for line in responses:
            if (not line.startswith("[others]") and (line != "[")):
                line = line.replace("[me] ", "",1)
                line = line.replace("[me]", "",1)
                response.append(line)
            if line.startswith("[others]") and not line.startswith("[others] " + userinput):
                print("done",response)
                return response
        if responses:
            answered = True
    print("done",response)
    return response


@app.route('/', methods=['GET'])
@cross_origin()
def home():
    return '''<h1>AI GPT-2 API</h1>
<p>Go Figure it out yourself, there is no documentation :)</p>'''

@app.route('/api/v1/ai/info', methods=['GET'])
@cross_origin()
def api_all():
    return jsonify([{"FirstName":FirstName,"Engine":Engine,"GPT_VARIANT_NAME":GPT_VARIANT_NAME,"TRAINING_STEPS":TRAINING_STEPS,"TRAINING_DATA_SIZE":TRAINING_DATA_SIZE,"DEVELOPMENT":DEVELOPMENT,"VERSION":VERSION,"LAST_UPDATE":LAST_UPDATE}])

@app.route('/api/v1/ai/communicate', methods=['GET'])
@cross_origin()
def api_id():
    context=False
    if 'msg' in request.args:
        msg = request.args['msg']
    else:
        return "Error: No msg field provided. Please specify a message."
    if 'context' in request.args:
        print("\n\nGOT CONTEXT\n\n")
        context = request.args['context']

    # Create an empty list for our results
    results = []

    return jsonify(communicate(msg,context,msg))

app.run(host='0.0.0.0', port=8081)
