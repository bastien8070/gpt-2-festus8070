import gpt_2_simple as gpt2
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import sys

def talk(userinput):
	#%reset -f
	try:
		os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
		tf.reset_default_graph()
	except:
		pass
	model_name = "345M"
	#sess = tf.Session(config=config)
	sess = gpt2.start_tf_sess()
	gpt2.load_gpt2(sess)
	text = gpt2.generate(sess, model_name=model_name, return_as_list=True ,prefix=userinput, length=35)[0]
	return text

for i in range(10):
	userinput = input("✍  > ")
	if userinput == "exit":
		exit(0)
	new_line = "[others] " + userinput
	if new_line.endswith(".") or new_line.endswith("?") or new_line.endswith("!"):
		new_line += "."


	response = []
	i = 0
	answered = False
	while answered == False:
		i+=1
		response = []
		if i == 3:
			response.append("zZz")
			answered = True		
		responses = talk(new_line).splitlines()
		for line in responses:
			if line.startswith("[me]"):
				response.append(line)
		if responses:
			answered = True
		
	for line in response:
		print(line)

